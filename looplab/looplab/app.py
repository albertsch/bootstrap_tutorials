import os

from flask import flash, Flask, jsonify, logging, redirect, render_template, \
                  request, session, url_for
from functools import wraps
from passlib.hash import sha256_crypt
from wtforms import Form, PasswordField, StringField, TextAreaField, validators
from wtforms.fields.html5 import EmailField
#from flask.ext.scss import Scss

app = Flask(__name__)
#Scss(app)
app.config['DEBUG'] = True
app.config['SECRET_KEY'] = 'gokumohandas_secret_key'
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0 # cache busting



# Coming soon page
@app.route('/', methods=['GET'])
def coming_soon():
    """Coming soon main page.
    """

    return render_template('index.html')

@app.errorhandler(404)
def page_not_found(e):
    """Redirect all nonexistent URLS.
    """
    return render_template('coming_soon.html')

if __name__ == '__main__':
    app.run(host= '0.0.0.0', port=5000)